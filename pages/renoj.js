import { useState } from "react";
import Head from "next/head";
import Link from "next/link";

import s from "./renoj.module.css";
import * as gtag from "../lib/gtag";

const Renoj = () => {
  // const [isIran, setIsIran] = useState(false);
  const isIran = false;

  // useEffect(() => {
  //   fetch("https://freegeoip.app/json/")
  //     .then((res) => res.json())
  //     .then((res) => {
  //       if (res.country_name === "Iran" || res.country_code === "IR") {
  //         setIsIran(true);
  //       }
  //     })
  //     .catch(() => false);
  // }, []);

  return (
    <div className={s.container}>
      <Head>
        <title>Renoj App</title>
        <link rel="icon" href="/renoj.ico" />
        <meta
          name="description"
          content="Fast to-do task management in Desktop for ultimate productivity. Manage your day-to-day like a boss."
        />
        <meta
          name="keywords"
          content="todo, task, fast todo, mac todo, windows todo, mac tasklist, windows tasklist, minimal, minimal task todo app, todo app, Todo, Renoj Todo, Renoj, renoj app, renoj, ribal dev, application for manage task, productivity, focus task , focus on task, more productivity, multi theme task, menu bar, tray app"
        />
        <meta name="image" content="./media/preview.png" />
        <meta itemProp="name" content="Renoj App" />
        <meta
          itemProp="description"
          content="Fast to-do task management in Desktop for ultimate productivity. Manage your day-to-day like a boss."
        />
        <meta itemProp="image" content="./media/preview.png" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Renoj App" />
        <meta
          name="twitter:description"
          content="Fast to-do task management in Desktop for ultimate productivity. Manage your day-to-day like a boss."
        />
        <meta name="twitter:site" content="@ribal_dev" />
        <meta name="twitter:image:src" content="./media/preview.png" />
        <meta name="og:title" content="Renoj App" />
        <meta
          name="og:description"
          content="Fast to-do task management in Desktop for ultimate productivity. Manage your day-to-day like a boss."
        />
        <meta name="og:image" content="./media/preview.png" />
        <meta name="og:url" content="https://ribal.dev/renoj" />
        <meta name="og:site_name" content="Renoj App" />
        <meta name="og:type" content="product" />
        <meta name="product:price:amount" content="3.99" />
        <meta name="product:brand" content="Ribal Dev" />
      </Head>
      <div className={"container"}>
        <nav className={s.nav}>
          <Link href="/renoj">
            <a className={s.nav__logo}>
              <img src={"./media/logo.svg"} alt="logo" />
              <h1>Renoj</h1>
            </a>
          </Link>
          <ul className={s.nav__list}>
            <li>
              <a href="#features">Features</a>
            </li>
            <li>
              <a href="#about">About</a>
            </li>
            <li>
              <a
                href="mailto:ribaldev@gmail.com?subject=&body="
                target="_blank"
              >
                Contact
              </a>
            </li>
          </ul>
          <a className={s.nav__dl} href="#get">
            Get Renoj
          </a>
        </nav>
      </div>
      <header className={s.header}>
        <div className={s.header__meta}>
          <h2>Simplify your daily todos.</h2>
          <p className={s.desc}>
            Fast to-do task management in Desktop for ultimate productivity.
            Manage your day-to-day like a boss.
          </p>
          <div className={s.buttonsDL}>
            <a className={s.header__dl} href="#get">
              Get Renoj Now
            </a>
            <a
              href="https://www.producthunt.com/posts/renoj-app?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-renoj-app"
              target="_blank"
            >
              <img
                src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=272148&theme=light"
                alt="Renoj App - Fast to-do task management for ultimate productivity. | Product Hunt"
                width="250"
                height="54"
              />
            </a>
          </div>
          <span className={s.header__dlDesc}>For Mac, Windows </span>
        </div>
        <div className={s.header__vedio}>
          <video loop autoPlay muted>
            <source src={"./media/Intro.mp4"} type="video/mp4" />
          </video>
        </div>
      </header>
      <main id="features" className={s.content}>
        <section className={s.features}>
          <h3>Features</h3>
          <ul>
            {[
              {
                title: "Focus on Today",
                desc: "Don't get distracted by giant loads of task lists.",
              },
              {
                title: "Fast Add",
                desc: "Use Control+Space+T shortcut to add your todo in an instant.",
              },
              {
                title: "Multi Theme",
                desc: "Light or dark, the choice is yours.",
              },
              {
                title: "Reminder",
                desc: "Get notified of your task's due time.",
              },
              {
                title: "Productivity",
                desc: "Monitor your productivity stats on a weekly basis.",
              },
              {
                title: "Accessible",
                desc: "Easily access the app's interface on Menu Bar",
              },
            ].map((item) => (
              <li key={item.title}>
                <h4>{item.title}</h4>
                <p>{item.desc}</p>
              </li>
            ))}
          </ul>
        </section>
        <section id="get" className={s.pricing}>
          <h3>Get Renoj</h3>
          <p className={s.preicing__desc}>
            You will have access to all features & new updates that boosts your
            productivity.
          </p>
          <div className={s.pricing__btns}>
            {isIran ? (
              <a
                href={"https://tehranreact.ir/public/renoj-mac-v1.dmg"}
                download="renoj-mac-v1.dmg"
                onClick={() => {
                  gtag.event({
                    category: "User",
                    action: "Download Mac V1",
                  });
                }}
              ></a>
            ) : (
              <a
                href={"https://gum.co/renojapp?wanted=true"}
                target="_blank"
                data-gumroad-single-product="true"
                onClick={() => {
                  gtag.event({
                    category: "User",
                    action: "Buy Mac V1",
                  });
                }}
              ></a>
            )}
            {isIran ? (
              <a
                className={s.win}
                href={"https://tehranreact.ir/public/renoj-win-v1.exe"}
                download="renoj-win-v1.exe"
                onClick={() => {
                  gtag.event({
                    category: "User",
                    action: "Download Win V1",
                  });
                }}
              ></a>
            ) : (
              <a
                className={s.win}
                href={"https://gum.co/renojapp?wanted=true"}
                target="_blank"
                data-gumroad-single-product="true"
                onClick={() => {
                  gtag.event({
                    category: "User",
                    action: "Buy Win V1",
                  });
                }}
              ></a>
            )}
          </div>
          {isIran ? (
            <p className={s.preicing__price}>Free forever for the persians</p>
          ) : (
            <p className={s.preicing__price}>Pay $3.99 for a Lifetime access</p>
          )}
        </section>
        <section id="about" className={s.about}>
          <div className="container">
            <div className={s.about__wrapper}>
              <Link href="/">
                <a className={s.company}>
                  <img src="./media/ribaldev.svg" alt="company logo" />
                  <p>Creative Product Development Agency</p>
                </a>
              </Link>
              <div className={s.socials}>
                <a target="blank" href="https://www.instagram.com/ribal.dev/">
                  Instagram
                </a>
                <a target="blank" href="https://twitter.com/ribal_dev/">
                  Twitter
                </a>
              </div>
            </div>
          </div>
        </section>
      </main>
    </div>
  );
};

export default Renoj;
