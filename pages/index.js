import Head from "next/head";

import s from "./index.module.css";
import Renoj from "./renoj";

export default function Home() {
  return (
    <div className={s.container}>
      <Head>
        <title>Ribal Dev</title>
        <link rel="icon" href="/favicon.ico" />
        <meta
          name="description"
          content="Creative Product Development Agency"
        />
        <meta name="image" content="./ribaldev.png" />
        <meta
          name="keywords"
          content="todo, task, fast todo, mac todo, windows todo, mac tasklist, windows tasklist, minimal, minimal task todo app, todo app, Todo, Renoj Todo, Renoj, renoj app, renoj, ribal dev, application for manage task, productivity, focus task , focus on task, more productivity, multi theme task, menu bar, tray app"
        />
        <meta itemProp="name" content="Ribal Dev" />
        <meta
          itemProp="description"
          content="Creative Product Development Agency"
        />
        <meta itemProp="image" content="./ribaldev.png" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Ribal Dev" />
        <meta
          name="twitter:description"
          content="Creative Product Development Agency"
        />
        <meta name="twitter:site" content="@ribal_dev" />
        <meta name="twitter:image:src" content="./ribaldev.png" />
        <meta name="og:title" content="Ribal Dev" />
        <meta
          name="og:description"
          content="Creative Product Development Agency"
        />
        <meta name="og:image" content="./ribaldev.png" />
        <meta name="og:url" content="https://ribal.dev/renoj" />
        <meta name="og:site_name" content="Ribal Dev" />
      </Head>
      <main className={s.main}>
        <img src="./media/ribaldev.svg" />
        <h1>Ribal Dev</h1>
        <p>Creative Product Development Agency</p>
        <div className={s.socials}>
          <a target="blank" href="https://www.instagram.com/ribal.dev/">
            Instagram
          </a>
          <a target="blank" href="https://twitter.com/ribal_dev/">
            Twitter
          </a>
        </div>
      </main>
      <Renoj />
    </div>
  );
}
