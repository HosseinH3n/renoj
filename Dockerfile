FROM node:latest as build-image

RUN mkdir /app
COPY package.json /app
COPY yarn.lock /app
COPY . /app
WORKDIR /app

RUN yarn

RUN yarn build

EXPOSE 3000

CMD [ "yarn" , "start" ]